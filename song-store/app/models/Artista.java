package models;

import java.util.HashSet;
import java.util.Set;

public class Artista  {

	public String id;
	public String nombre;
	public String trayectoria;
	
	
	public Artista(){
		
	}
	
	public Artista (String id, String nombre, String trayectoria) {
		
		this.id = id;
		this.nombre = nombre;		
		this.trayectoria = trayectoria;
		
	}
	
	
	private static Set<Artista> artistas;
	static{
		artistas = new HashSet<>();
		
	}

	
	public static Set<Artista>todosArtistas(){
		return artistas;
	}
	
	/*public static Artista buscarPorId(String id){
		for (Artista artista : artistas){
			if(id.equals(artista.id)){
				return artista;
			}
		}
		return null;
	}*/
	
	
	public static void add(Artista artista){
		
		artistas.add(artista);
	}

}	

